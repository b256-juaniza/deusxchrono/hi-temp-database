require('dotenv').config();
const socketIo = require('socket.io');

let io;

const init = (server) => {
    io = socketIo(server, {
        cors: {
            origin: process.env.WEBSOCKET_ORIGIN, // Allow access from this origin
            methods: ["GET", "POST", "PATCH", "PUT"],
            credentials: true
        }
    });

    io.on('connection', (socket) => {
        console.log('A user connected');
        
        // Handle socket events
        // Example: socket.on('event', handleEvent);

        socket.on('disconnect', () => {
            console.log('User disconnected');
        });
    });

    return io;
};

const getIO = () => {
    if (!io) {
        throw new Error('Socket.IO not initialized!');
    }
    return io;
};

module.exports = { init, getIO };
