const multer = require('multer');
const { CloudinaryStorage } = require('multer-storage-cloudinary');
const cloudinary = require('./cloudinary'); // Ensure correct path to cloudinary configuration

// Define the storage configuration
const storage = new CloudinaryStorage({
  cloudinary: cloudinary,
  params: async (req, file) => {
    // Ensure file format matches Cloudinary's expectations
    const format = fileFormat(file);

    return {
      folder: 'Reports', // Specify the folder in Cloudinary
      format: format, // Set format based on MIME type
      public_id: `${Date.now()}-${file.originalname}`, // Customize public ID
      resource_type: getResourceType(file.mimetype) // Determine resource type
    };
  }
});

// Define file format logic based on MIME type
const fileFormat = (file) => {
  switch (file.mimetype) {
    case 'image/png':
      return 'png';
    case 'image/jpeg':
      return 'jpg';
    case 'application/pdf':
      return 'pdf';
    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
      return 'docx';
    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
      return 'xlsx';
    default:
      // For unknown types, fallback to auto
      return 'auto';
  }
};

// Determine resource type for Cloudinary
const getResourceType = (mimetype) => {
  if (mimetype.startsWith('image/')) {
    return 'image';
  } else if (mimetype.startsWith('application/pdf')) {
    return 'raw';
  } else if (mimetype.startsWith('application/vnd.openxmlformats-officedocument')) {
    return 'raw'; // Set resource type to 'raw' for docx and xlsx
  } else {
    return 'auto';
  }
};

const upload = multer({ storage: storage });

module.exports = upload;
