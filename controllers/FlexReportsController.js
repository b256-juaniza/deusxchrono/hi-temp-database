const EmployeeDepartmentReport = require("../models/FlexReport");
const cloudinary = require("../config/cloudinary");
const { getIO } = require('../socket.js');

function formattedDate(date, includeTime = false) {
    // Adjust to Philippines Standard Time (UTC+8)
    date.setHours(date.getHours());

    let month = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month;
    let day = date.getDate();
    day = day < 10 ? '0' + day : day;
    let year = date.getFullYear();

    let formattedDate = month + '-' + day + '-' + year;

    if (includeTime) {
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // Handle midnight (0 hours)
        minutes = minutes < 10 ? '0' + minutes : minutes;
        let timeString = hours + ':' + minutes + ' ' + ampm;
        formattedDate += ' ' + timeString;
    }
    return formattedDate;
}

exports.createReport = async ({ employeeName, department, data, fileUrls }) => {
  const io = getIO(); // Assuming getIO() retrieves the socket.io instance
  try {
    // Create new report instance with data and fileUrls
    const newReport = new EmployeeDepartmentReport({
      employeeName,
      department,
      data,
      attachments: fileUrls // Assign fileUrls directly to the attachments field
    });

    // Save report to database
    await newReport.save();

    // Emit "NewReport" event to notify clients about the new report
    io.emit('NewReport', newReport);

    // Return newly created report
    return newReport;
  } catch (error) {
    console.error('Error creating report:', error);
    throw error; // Throw the error to handle it in the calling function
  }
};


exports.getReport = async (reportId) => {
    try {
        // Find the report by ID
        const report = await EmployeeDepartmentReport.findById(reportId);

        // Check if report exists
        if (!report) {
            console.log('Report not found');
        }

        // Return the found report
        return report;
    } catch (error) {
        console.log(`Failed to retrieve report: ${error}`);
    }
};

exports.getReportsByDepartment = async (department) => {
    try {
        // Find all reports by department
        const reports = await EmployeeDepartmentReport.find({ department });

        // Check if reports exist
        if (reports.length === 0) {
            throw new Error('No reports found for the specified department');
        }

        // Return the found reports
        return reports;
    } catch (error) {
        console.log(error);
        return false;
    }
};

exports.updateFeedback = async (req) => {
    const { feedback } = req;

    try {
        // Find the report by ID and update the feedback
        const updatedReport = await EmployeeDepartmentReport.findByIdAndUpdate(
            req.id,
            { feedback: feedback },
            { new: true, runValidators: true }
        );

        if (!updatedReport) {
            return "Report Not Found!";
        }

        return updatedReport;
    } catch (error) {
        console.error(error);
        return false;
    }
};

exports.deleteReport = async (req) => {
    const { id } = req;

    try {
        // Find the report by ID and delete it
        const deletedReport = await EmployeeDepartmentReport.findByIdAndDelete(id);

        // Check if the report was found and deleted
        if (!deletedReport) {
            return "Report Not Found!";
        }

        // Return a success message or the deleted report
        return true;
    } catch (error) {
        console.error(error);
        return false;
    }
};

