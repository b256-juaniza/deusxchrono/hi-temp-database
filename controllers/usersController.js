const Users = require("../models/Users.js");
const CryptoJS = require('crypto-js');
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Tasks = require("../models/Tasks.js");
const TaskTypes = require("../models/TaskTypes.js");
const { getIO } = require('../socket.js');
// Define handleProfileUpload function here...

const secretKey = 'Secret-Hi-Temp-Key';

module.exports.handleProfileUpload = async (file) => {
  const formData = new FormData();
  formData.append('file', file);
  formData.append('upload_preset', 'profile_upload'); // Use your Cloudinary upload preset name

  try {
    const response = await fetch(`https://api.cloudinary.com/v1_1/dgzhcuwym/image/upload`, {
      method: 'POST',
      body: formData
    });

    if (!response.ok) {
      throw new Error('Failed to upload image to Cloudinary');
    }

    const data = await response.json();
    if (data.secure_url) {
      return data.secure_url; // Return the uploaded image URL
    }
    throw new Error('No secure URL found in Cloudinary response');
  } catch (error) {
    console.error('Error uploading image to Cloudinary:', error);
    throw error; // Propagate the error to the caller
  }
};

module.exports.registerUser = async (requestBody) => {
  try {
    const encryptedPassword = CryptoJS.AES.encrypt(requestBody.password, secretKey).toString();

    let newUser = new Users({
      name: requestBody.name,
      username: requestBody.username,
      department: requestBody.department,
      role: requestBody.role,
      password: encryptedPassword
    });

    // Upload image to Cloudinary and set the profile field with the URL
    if (requestBody.profile) {
      const profileUrl = await module.exports.handleProfileUpload(requestBody.profile);
      newUser.profile = profileUrl;
    }

    await newUser.save();
    return true;
  } catch (error) {
    console.error('Error registering user:', error);
    return error;
  }
};

module.exports.changePassword = async (reqBody) => {
  try {
    const user = await Users.findOne({ username: reqBody.username });
    if (!user) {
      return { success: false, message: 'User not found' };
    }

    // Check if changePassword flag is set to true
    if (!user.changePassword) {
      return { success: false, message: 'Password change is not allowed for this user' };
    }

    const encryptedNewPassword = CryptoJS.AES.encrypt(reqBody.newPassword, secretKey).toString();
    user.password = encryptedNewPassword;
    user.changePassword = false;
    await user.save();

    return { success: true, message: 'Password changed successfully' };
  } catch (error) {
    console.error("Error changing password:", error);
    return { success: false, message: 'Error changing password' };
  }
};

module.exports.updateEmployee = async (req, res) => {
    const { _id, name, username, department, role, changePassword, isAdmin } = req.body;

    if (!_id) {
        return res.status(400).json({ success: false, message: 'Employee ID is required' });
    }

    try {
        const user = await Users.findByIdAndUpdate(
            _id,
            { name, username, department, role, changePassword, isAdmin },
            { new: true }
        );

        if (!user) {
            return { success: false, message: 'User not found' };
        }

        return { success: true, message: 'Employee updated successfully', user };
    } catch (error) {
        console.error('Error updating employee:', error);
        return { success: false, message: 'Internal server error' };
    }
};

const updateChangePassword = async (data) => {
    const { username, changePassword } = data;

    if (typeof changePassword !== 'boolean') {
        return { success: false, message: 'changePassword must be a boolean' };
    }

    try {
        const user = await Users.findOneAndUpdate(
            { username: username },
            { changePassword: changePassword },
            { new: true }
        );

        if (!user) {
            return { success: false, message: 'User not found' };
        }

        return { success: true, message: 'changePassword updated successfully', user };
    } catch (error) {
        console.error('Error updating changePassword:', error);
        return { success: false, message: 'Internal server error' };
    }
};

// Function to get all users' names, IDs, and departments
module.exports.getAllUserDetails = async () => {
  try {
    // Find all users and select name, _id, and department fields
    const users = await Users.find({}, 'name _id department Status');

    // Return the array of users with name, _id, and department
    return users;
  } catch (error) {
    console.error('Error fetching user details:', error);
    throw new Error('Failed to fetch user details');
  }
};

module.exports.logoutUser = async (userId) => {
    try {
        const io = getIO(); // Import io only when needed
        
        // Update user's status to false
        await Users.findOneAndUpdate({ _id: userId }, { Status: false });

        // Emit "userStatusChange" event
        io.emit('userStatusChange', { userId: userId, status: false });

        return true; // Successfully logged out
    } catch (error) {
        console.error("Error in logout:", error);
        return false; // Handle errors gracefully
    }
};

module.exports.authenticateUser = async (requestBody, res) => {
  try {
    const io = getIO(); // Import io only when needed

    const result = await Users.findOne({ username: requestBody.username });
    if (!result) {
      return false;
    }

    const decryptedPassword = CryptoJS.AES.decrypt(result.password, secretKey).toString(CryptoJS.enc.Utf8);
    if (decryptedPassword === requestBody.password) {
      // Update user's status to true
      await Users.findOneAndUpdate({ username: requestBody.username }, { Status: true });

      // Emit event to notify clients that a user has logged in
      io.emit('userStatusChange', { userId: result._id, status: true }); // Emit userLoggedIn event with userId

      const accessToken = auth.createAccessToken(result);

      return {access: accessToken};
    } else {
        return false;
    }
  } catch (error) {
    return false;
  }
};


module.exports.getProfile = (data) => {
	return Users.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

module.exports.userProfile = (data) => {
    return Users.findOne({ name: data.name }, { password: 0 }).then(result => {
        return result;
    });
};

module.exports.userDetails = (data) => {
    return Users.findOne({ username: data.username }, { changePassword: 1 }).then(result => {
        return result;
    });
};

module.exports.allusers = () => {
	return Users.find({}).then(result => {
		return result;
	});
};

module.exports.checkIfEmailExists = (requestBody) => {
	return Users.find({name: requestBody.name}).then(result => {
		if(result.length > 0) {
			return true;
		} else {
			return false;
		}
	})
};

module.exports.updateProfile = (id, requestBody) => {
	return Users.findByIdAndUpdate(id, {profile: requestBody.profile}).then((result, err) => {
		if (err) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.getUserbyDepartment = (requestBody) => {
	return Users.find({department: requestBody.department}, {name: 1}).then((result, err) => {
		if (err) {
			return false;
		} else {
			return result;
		}
	})
}

module.exports.getTaskTypebyDepartment = (requestBody) => {
	return TaskTypes.findOne({department: requestBody.department}, {taskTypes: 1}).then((result, err) => {
		if (err) {
			return false;
		} else {
			return result;
		}
	})
}

module.exports.getDepartment = () => {
	return TaskTypes.find({}).then((result, err) => {
		if (err) {
			return false;
		} else {
			return result;
		}
	})
}