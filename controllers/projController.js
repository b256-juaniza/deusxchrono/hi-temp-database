const Tasks = require("../models/Tasks.js");
const Users = require("../models/Users.js");
const Project = require("../models/Project.js");
const mongoose = require('mongoose');
const { getIO } = require('../socket.js');

function formatDate(date) {
    let month = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month;
    let day = date.getDate();
    day = day < 10 ? '0' + day : day;
    let year = date.getFullYear();

    let hours = date.getHours();
    hours = hours < 10 ? '0' + hours : hours;
    let minutes = date.getMinutes();
    minutes = minutes < 10 ? '0' + minutes : minutes;

    return month + '-' + day + '-' + year + ' ' + hours + ':' + minutes;
}

module.exports.createProject = (requestBody) => {
    const io = getIO();
    let now = new Date();
    let formattedDate = formatDate(now);

    let newProj = new Project({
        projectName: requestBody.projectName,
        description: requestBody.description,
        company: requestBody.company,
        product: requestBody.product,
        address: requestBody.address,
        createdOn: formattedDate
    });

    return newProj.save()
        .then((proj) => {

            io.emit('ProjectUpdated', {projectName: requestBody.projectName});
            
            return proj; // Return the saved task if successful
        })
        .catch((err) => {
            console.error("Error saving Project:", err);
            throw err; // Re-throw the error to be handled by the caller
        });
};

module.exports.updateProjectTasks = async (requestBody) => {
    const io = getIO();
    try {
        if (requestBody.tasksId && requestBody.projectName) {
            const project = await Project.findOne({ projectName: requestBody.projectName });

            if (!project) {
                throw new Error("Project not found");
            }
            
            const isTaskIdExist = project.subTasks.some(subTask => subTask.TaskId === requestBody.tasksId);

            // If the task ID doesn't exist, add it
            if (!isTaskIdExist) {
                project.subTasks.push({ TaskId: requestBody.tasksId });
            }

            // Save the project
            await project.save();

            io.emit('ProjectUpdated', {projectName: project.projectName});

            return project.subTasks;
        } else {
            throw new Error("Invalid payload: Missing tasks or projectName");
        }
    } catch (error) {
        console.error("Error updating project tasks:", error);
        throw new Error('Error updating project tasks');
    }
};

module.exports.updateProject = (reqbody) => {
    const io = getIO();
    let modifyProject = {
        projectName: reqbody.projectName,
        description: reqbody.description,
        company: reqbody.company,
        product: reqbody.product,
        address: reqbody.address,
        Status: reqbody.Status,
        Remarks: reqbody.Remarks,
        DateCompleted: new Date()
    }

    return Project.findOneAndUpdate({projectName : reqbody.projectName}, modifyProject).then((result, err) => {
        if (err) {
            return err;
        } else {
            io.emit('ProjectUpdated', {projectName: result.projectName});
            return result;
        }
    })
}

module.exports.subTask = async (reqbody) => {
    return await Project.findOne({ _id: reqbody.id }).then(results => {

        if (!results) {
            throw new Error('Project not found!');
        }

        results.subTasks.push(reqbody.subTask);
        results.save().then((res, err) => {
            if (err) {
                return err;
            } else {
                return res;
            }
        })
    })   
};

module.exports.checkIfProjectExists = (requestBody) => {
    return Project.find({projectName: requestBody.projectName}).then(result => {
        if(result.length > 0) {
            return true;
        } else {
            return false;
        }
    })
};

module.exports.getAll = () => {
    return Project.find({}).then(result => {
        return result;
    })
}

module.exports.getDetails = (reqbody) => {
    return Project.findOne({projectName: reqbody.projectName}).then(result => {
        return result;
    })
}

module.exports.availableProjects = async (fullName) => {
    try {
        const result = await Project.aggregate([
            // Unwind the subTasks array
            { $unwind: "$subTasks" },
            // Group by project fields and push all subtasks into an array
            {
                $group: {
                    _id: "$_id",
                    projectName: { $first: "$projectName" },
                    description: { $first: "$description" },
                    company: { $first: "$company" },
                    product: { $first: "$product" },
                    address: { $first: "$address" },
                    createdOn: { $first: "$createdOn" },
                    isActive: { $first: "$isActive" },
                    Status: { $first: "$Status" },
                    Remarks: { $first: "$Remarks" },
                    projExpenses: { $first: "$projExpenses" },
                    DateCompleted: { $first: "$DateCompleted" },
                    subTasks: { $push: "$subTasks" }
                }
            }
        ]);

        return result;
    } catch (error) {
        // Handle errors, such as database query errors
        console.error('Error fetching available projects:', error);
        throw new Error('Failed to fetch available projects');
    }
};

module.exports.updateStatus = async (id, status) => {
    const io = getIO();
    try {
        const result = await Project.findByIdAndUpdate(id, { isActive: status }, { new: true });
        
        if (!result) {
            throw new Error('Project not found');
        }
        io.emit('ProjectUpdated', {projectName: result.projectName});
        return true;
    } catch (error) {
        // Handle errors, such as database query errors
        console.error('Error updating project status:', error);
        return false;
    }
}

// Add a chat to a project
exports.addChat = async (reqbody) => {
    const io = getIO();

    const { id, name, message, timestamp} = reqbody;

    try {
        const project = await Project.findOne({projectName: reqbody.projectName});

        if (!project) {
            return ({ error: 'Project not found' });
        }

        project.chats.push({ id, name, message, timestamp});
        await project.save();

        io.emit('ChatUpdated', {name: name, projectName: project.projectName});

        return ({ message: 'Chat added successfully', project });
    } catch (error) {
        return ({ error: 'Server error', error });
    }
};

// Get all chats of a project
exports.getChats = async (reqbody) => {

    try {
        const project = await Project.findOne({projectName: reqbody.projectName});
        if (!project) {
            return ({ error: 'Project not found' });
        }

        return (project.chats);
    } catch (error) {
        return ({ error: 'Server error' });
    }
};

// Update a chat in a project
exports.updateChat = async (req, res) => {
    const { projectId, chatId } = req.params;
    const { id, name, message, uploads } = req.body;

    try {
        const project = await Project.findById(projectId);
        if (!project) {
            return ({ error: 'Project not found' });
        }

        const chat = project.chats.id(chatId);
        if (!chat) {
            return ({ error: 'Chat not found' });
        }

        chat.id = id || chat.id;
        chat.name = name || chat.name;
        chat.message = message || chat.message;
        chat.uploads = uploads || chat.uploads;

        await project.save();

        return ({ message: 'Chat updated successfully', project });
    } catch (error) {
        return ({ error: 'Server error' });
    }
};

// Delete a chat from a project
exports.deleteChat = async (req, res) => {
    const { projectId, chatId } = req.params;

    try {
        const project = await Project.findById(projectId);
        if (!project) {
            return res.status(404).json({ error: 'Project not found' });
        }

        project.chats.id(chatId).remove();
        await project.save();

        res.status(200).json({ message: 'Chat deleted successfully', project });
    } catch (error) {
        res.status(500).json({ error: 'Server error' });
    }
};