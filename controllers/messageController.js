const User = require('../models/Users');
const Message = require('../models/Messages');
const { getIO } = require('../socket.js');

module.exports.saveMessage = async (senderId, recipientId, content, department) => {
    try {
        // Find the sender and recipient users
        const sender = await User.findById(senderId);
        const recipient = await User.findById(recipientId);

        if (!sender || !recipient) {
            throw new Error('Sender or recipient user not found');
        }

        // Create a new message document
        const message = new Message({
            sender: { id: senderId, name: sender.name }, // Ensure sender object is correctly formatted
            recipient: { id: recipientId, name: recipient.name }, // Ensure recipient object is correctly formatted
            content: content,
            timestamp: new Date(),
            department: department
        });

        // Save the message to the database
        await message.save();

        // Associate the message with the sender and recipient users
        if (sender.sentMessages && recipient.receivedMessages) {
            sender.sentMessages.push(message._id); // Push the message ID to the sender's sent messages array
            recipient.receivedMessages.push(message._id); // Push the message ID to the recipient's received messages array

            await sender.save();
            await recipient.save();
        } else {
            throw new Error('Sender or recipient sent/received messages array not found');
        }

        // Emit event to notify clients about the new message
        const io = getIO();
        io.emit('new_message', message);

        return message; // Return the saved message
    } catch (error) {
        console.error('Error saving message:', error);
        throw new Error('Failed to save message');
    }
};


module.exports.getAllConversations = async (userId1, userId2) => {
    try {
        const conversations = await Message.find({
            $or: [
                { 'sender.id': userId1, 'recipient.id': userId2 },
                { 'sender.id': userId2, 'recipient.id': userId1 }
            ]
        }).sort({ timestamp: 1 });

        return conversations; // Return the retrieved conversations
    } catch (error) {
        console.error('Error fetching conversations:', error);
        throw new Error('Failed to fetch conversations');
    }
};
