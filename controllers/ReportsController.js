const Tasks = require("../models/Tasks.js");
const Users = require("../models/Users.js");
const Project = require("../models/Project.js");
const Reports = require("../models/Reports.js");
const { getIO } = require('../socket.js');

function formatDate(date) {
    let month = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month;
    let day = date.getDate();
    day = day < 10 ? '0' + day : day;
    let year = date.getFullYear();

    return month + '-' + day + '-' + year;
}

function formattedDate(date, includeTime = false) {
    // Adjust to Philippines Standard Time (UTC+8)
    date.setHours(date.getHours());

    let month = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month;
    let day = date.getDate();
    day = day < 10 ? '0' + day : day;
    let year = date.getFullYear();

    let formattedDate = month + '-' + day + '-' + year;

    if (includeTime) {
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // Handle midnight (0 hours)
        minutes = minutes < 10 ? '0' + minutes : minutes;
        let timeString = hours + ':' + minutes + ' ' + ampm;
        formattedDate += ' ' + timeString;
    }
    return formattedDate;
}

module.exports.createReport = async (name, requestBody) => {

    const io = getIO();

    try {
        let now = new Date();
        let weekDates = [];
        
        // Generate week dates
        for (let i = 0; i < 5; i++) {
            let date = new Date(now);
            date.setDate(now.getDate() - (now.getDay() - 1) + i); // Adjust to Monday and add day index
            weekDates.push(date); // Add formatted date to array
        }

        // Create reports data for the week
        const reportsData = requestBody.reportData.map((dayData, index) => {
            let dayName = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'][index];
            const dayDetails = dayData || {}; // Get day details or an empty object if not present
            return {
                day: dayName,
                details: {
                    Customer: dayDetails.Customer || '',
                    Product: dayDetails.Product || '',
                    Remarks: dayDetails.Remarks || '',
                    Amount: dayDetails.Amount || '',
                    Date: formattedDate(weekDates[index], false),
                }
            };
        });
        
        const created_date = new Date();
        
        // Create a new report document
        const newReport = new Reports({
            employee: name,
            createdOn: formattedDate(created_date, true),
            reports: reportsData,
            attachment: requestBody.attachment // Set the attachment field from requestBody
        });

        // Save the new report to the database
        const savedReport = await newReport.save();

        // Emit "userStatusChange" event
        io.emit('NewReport', { name: name, date: formattedDate(created_date, true) });

        return savedReport; // Return the saved report
    } catch (err) {
        console.error("Error saving report:", err);
        throw err; // Re-throw the error to be handled by the caller
    }
};

module.exports.updateProjectTasks = async (requestBody) => {
    try {
        if (requestBody.tasks && requestBody.tasks.projectName) {
            const project = await Reports.findOne({ projectName: requestBody.tasks.projectName });

            if (!project) {
                throw new Error("Project not found");
            }

            const validStatuses = ["Completed", "In Progress", "Failed"];
            if (!validStatuses.includes(requestBody.Status)) {
                throw new Error("Invalid task status");
            }

            if (requestBody.Status === "Completed") {
                // Push the task into project's subTasks array
                project.subTasks.push(requestBody.tasks);
            } else {
                // Remove task with the same _id from project's subTasks
                project.subTasks = project.subTasks.filter(subTask => !subTask._id.equals(requestBody.tasks._id));
            }

            // Save the project
            await project.save();

            return project.subTasks;
        } else {
            throw new Error("Invalid payload: Missing tasks or projectName");
        }
    } catch (error) {
        console.error("Error updating project tasks:", error);
        throw new Error('Error updating project tasks');
    }
};

module.exports.updateProject = (reqbody) => {

    let modifyProject = {
        projectName: reqbody.projectName,
        description: reqbody.description,
        company: reqbody.company,
        address: reqbody.address,
        Status: reqbody.Status,
        Remarks: reqbody.Remarks,
        DateCompleted: new Date()
    }

    return Reports.findOneAndUpdate({projectName : reqbody.projectName}, modifyProject).then((result, err) => {
        if (err) {
            return err;
        } else {
            return result;
        }
    })
}

module.exports.subTask = async (reqbody) => {
    return await Reports.findOne({ _id: reqbody.id }).then(results => {

        if (!results) {
            throw new Error('Project not found!');
        }

        results.subTasks.push(reqbody.subTask);
        results.save().then((res, err) => {
            if (err) {
                return err;
            } else {
                return res;
            }
        })
    })   
};

module.exports.checkIfProjectExists = (requestBody) => {
    return Reports.find({projectName: requestBody.projectName}).then(result => {
        if(result.length > 0) {
            return true;
        } else {
            return false;
        }
    })
};

module.exports.getAll = () => {
    return Reports.find({}).sort({ createdOn: 'desc' }).then(result => {
        return result;
    });
}


module.exports.getUsersReport = (reqbody) => {
    return Reports.find({employee: reqbody.employee}).then(result => {
        return result;
    })
}


module.exports.availableProjects = async (fullName) => {
    try {
        const result = await Reports.aggregate([
            // Unwind the subTasks array
            { $unwind: "$subTasks" },
            // Group by project fields and push all subtasks into an array
            {
                $group: {
                    _id: "$_id",
                    projectName: { $first: "$projectName" },
                    description: { $first: "$description" },
                    company: { $first: "$company" },
                    address: { $first: "$address" },
                    createdOn: { $first: "$createdOn" },
                    isActive: { $first: "$isActive" },
                    Status: { $first: "$Status" },
                    Remarks: { $first: "$Remarks" },
                    projExpenses: { $first: "$projExpenses" },
                    DateCompleted: { $first: "$DateCompleted" },
                    subTasks: { $push: "$subTasks" }
                }
            }
        ]);

        return result;
    } catch (error) {
        // Handle errors, such as database query errors
        console.error('Error fetching available projects:', error);
        throw new Error('Failed to fetch available projects');
    }
};
