const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    profile: {
        type: String,
        default: "https://res.cloudinary.com/dgzhcuwym/image/upload/v1719498551/Hi-Temp_Profiles/hc4e8se4nktwblghov1w.jpg"
    },
    Status: {
        type: Boolean,
        default: false
    },
    name: {
        type: String,
        required: [true, "First Name is Required"]
    },
    username: {
        type: String,
        required: [true, "username is Required"]
    },
    password: {
        type: String,
        required: [true, "Password is Required"]
    },
    changePassword: {
        type: Boolean,
        default: false
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    department: {
        type: String,
        required: [true, "Department is Required"]
    },
    role: {
        type: String,
        default: "Employee"
    },
    Tasks: [
        {
            objectId: {
                type: String
            },
            taskType: {
                type: String
            },
            assignedOn: {
                type: String,
                default: new Date()
            },
            active: {
                type: Boolean,
                default: true
            }
        }
    ],
    areas: [
        {
            address: {
                type: String
            }
        }
    ],
    sentMessages: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Messages'
        }
    ],
    receivedMessages: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Messages'
        }
    ]
});

module.exports = mongoose.model("Users", userSchema);
