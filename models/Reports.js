const mongoose = require("mongoose");

const reportSchema = new mongoose.Schema({
    employee: {
        type: String,
        required: [true, "Employee is Required"]
    },
    createdOn: {
    	type: Date
    },
    reports: [
        {
            day: {
                type: String,
                enum: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
                required: true
            },
            details: {
                Customer: {
                    type: String
                },
                Product: {
                    type: String
                },
                Remarks: {
                    type: String
                },
                Amount: {
                    type: String
                },
                Date: {
                    type: String
                }
            }
        }
    ],
    attachment: {
        type: String
    }
});

module.exports = mongoose.model("Reports", reportSchema);
