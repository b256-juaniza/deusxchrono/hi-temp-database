const mongoose = require("mongoose");

const projectSchema = new mongoose.Schema({
	projectName: {
		type: String,
		required: [true, "Project Name is Required"]
	},
	description: {
		type: String,
		required: [true, "Description is Required"]
	},
	company: {
		type: String,
		required: [true, "Company Name is Required"]
	},
	product: {
		type: String,
		required: [true, "Product Name is Required"]
	},
	address: {
		type: String,
		required: [true, "Address is Required"]
	},
	createdOn: {
		type: Date
	},
	isActive: {
		type: Boolean,
		default: true
	},
	Status: {
		type: String,
		default: "In Progress"
	},
	Remarks: {
		type: String
	},
	projExpenses:{
		type: String
	},
	DateCompleted:{
		type: Date
	},
	subTasks: [
		{
			TaskId: {
				type: String,
				required: [true, "TaskId is Required"]
			}
		}
	],
	chats: [{
		id: {
			type: String
		},
		name: {
			type: String
		},
		message: {
			type: String
		},
		timestamp: {
			type: String
		}
	}],
	uploads: [{
		link: {
			type: String
		}
	}]
});

module.exports = mongoose.model("Project", projectSchema);