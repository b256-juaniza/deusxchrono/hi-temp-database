const mongoose = require("mongoose");

const reportSchema = new mongoose.Schema({
    employeeName: {
        type: String,
        required: true
    },
    department: {
        type: String,
        required: true
    },
    createdOn: {
        type: Date,
        default: Date.now
    },
    data: {
        type: [{
            columnHeaders: [String],
            rows: [{
                cells: [String]
            }]
        }],
        default: []
    },
    attachments: {
        type: [String] // Array of attachment URLs from Cloudinary
    },
    feedback: {
        type: String
    }
});

module.exports = mongoose.model("EmployeeReports", reportSchema);
