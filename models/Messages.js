const mongoose = require('mongoose');
const Users = require('./Users');

const messageSchema = new mongoose.Schema({
    sender: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users', // Reference to the User model
            required: true
        },
        name: {
            type: String,
            required: true
        }
    },
    recipient: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users', // Reference to the User model
            required: true
        },
        name: {
            type: String,
            required: true
        }
    },
    content: {
        type: String,
        required: true
    },
    timestamp: {
        type: Date,
        default: Date.now
    },
    department: {
        type: String,
        required: true
    }
});

const Messages = mongoose.model('Messages', messageSchema);

module.exports = Messages;
