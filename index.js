require('dotenv').config(); // Load dotenv configuration

const express = require("express");
const mongoose = require("mongoose");
const http = require('http');
const cors = require("cors");
const { init: initSocket } = require("./socket.js");
const userRoutes = require("./routes/userRoutes.js");
const taskRoutes = require("./routes/tasksRoutes.js");
const projRoutes = require("./routes/projRoutes.js");
const reportRoutes = require("./routes/FlexReportsRoutes.js");
const messageRoutes = require("./routes/messageRoutes.js");

const app = express();
const server = http.createServer(app);

// Middleware for parsing JSON and URL-encoded data
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const corsOptions = {
    origin: process.env.WEBSOCKET_ORIGIN,
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], // Add PATCH method here
    allowedHeaders: ['Content-Type', 'Authorization']
};
app.use(cors(corsOptions));

// Initialize Socket.IO
const io = initSocket(server);

// Use the middleware for WebSocket connections
io.use((socket, next) => {
    // Ensure you are checking against the correct origin
    const origin = socket.handshake.headers.origin;
    if (origin === process.env.WEBSOCKET_ORIGIN) {
        return next();
    }
    return next(new Error("Unauthorized access"));
});

// MongoDB connection
mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.on("error", console.error.bind(console, "connection error"));
mongoose.connection.once("open", () => console.log(`Connected to the cloud database`));

// Routes
app.use("/users", userRoutes);
app.use("/tasks", taskRoutes);
app.use("/project", projRoutes);
app.use("/reports", reportRoutes);
app.use("/messages", messageRoutes);

// Start the server
const PORT = process.env.PORT || 4000;
server.listen(PORT, () => console.log(`API is now online on port ${PORT}`));
