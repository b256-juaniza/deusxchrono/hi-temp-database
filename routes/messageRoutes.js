const express = require('express');
const router = express.Router();
const messageController = require('../controllers/messageController');
const auth = require("../auth.js");

router.post('/send', auth.verify, async (req, res) => {
    try {
        const decodedPayload = auth.decode(req.headers.authorization);
        const { id, name } = decodedPayload;

        const { recipientId, content, department } = req.body; // Assuming recipientId is included in the request body

        if (!recipientId || !content) {
            return res.status(400).json({ error: 'Recipient ID and message content are required' });
        }

        const message = await messageController.saveMessage(id, recipientId, content, department);
        res.status(201).json(message);
    } catch (error) {
        console.error('Error saving message:', error);
        res.status(500).json({ error: 'Failed to save message' });
    }
});

// Route to retrieve all conversations between two users
router.get('/conversation/:userId', auth.verify, async (req, res) => {
    try {
        const decodedPayload = auth.decode(req.headers.authorization);
        const { id } = decodedPayload; // Extract user ID from decoded payload
        const userId2 = req.params.userId; // Extract userId2 from URL parameter

        const conversations = await messageController.getAllConversations(id, userId2);
        res.status(200).json(conversations);
    } catch (error) {
        console.error('Error fetching conversations:', error);
        res.status(500).json({ error: 'Failed to fetch conversations' });
    }
});

module.exports = router;
