const express = require("express");
const router = express.Router();
const reportsController = require("../controllers/ReportsController.js");
const auth = require("../auth.js");

router.post("/generateReport", auth.verify, (req, res) => {
    const data = {
        id: auth.decode(req.headers.authorization).id,
        name: auth.decode(req.headers.authorization).name
    }
    if (data.id !== null && data.id !== undefined) {
        reportsController.createReport(data.name, req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send("Not an Authenticated!");
    }

})

router.post("/checkProject", (req, res) => {
    reportsController.checkIfProjectExists(req.body).then(resultFromController => res.send(resultFromController));
})


router.patch("/updateProject", auth.verify, (req, res) => {
    const data = {
        role: auth.decode(req.headers.authorization).role
    }

    if(data.role === "Admin") {
        reportsController.updateProject(req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send("Not an Admin!");
    }
})


router.get("/allReports", (req, res) => {
    reportsController.getAll().then(resultFromController => res.send(resultFromController));
})

router.post("/MyReports", (req, res) => {
    reportsController.getUsersReport(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/active", (req, res) => {
    reportsController.availableProjects(req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/updateProjectTasks", (req, res) => {
    reportsController.updateProjectTasks(req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;
