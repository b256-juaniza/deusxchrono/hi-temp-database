const express = require("express");
const router = express.Router();
const userController = require("../controllers/usersController.js");
const auth = require("../auth.js");

router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.patch('/change-password', async (req, res) => {
  try {
    const result = await userController.changePassword(req.body);
    if (result.success) {
      return res.status(200).json(result);
    } else {
      return res.status(400).json(result);
    }
  } catch (error) {
    console.error("Error changing password:", error);
    res.status(500).json({ success: false, message: 'Error changing password' });
  }
});

// New route for updating the employee data
router.patch('/update-employee', async (req, res) => {
    try {
        const result = await userController.updateEmployee(req, res);
        if (result.success) {
            return res.status(200).json(result);
        } else {
            return res.status(400).json(result);
        }
    } catch (error) {
        console.error('Error updating employee:', error);
        res.status(500).json({ success: false, message: 'Error updating employee' });
    }
});

// New route for updating the changePassword field
router.patch('/update-change-password', async (req, res) => {
    try {
        const result = await userController.updateChangePassword(req.body);
        if (result.success) {
            return res.status(200).json(result);
        } else {
            return res.status(400).json(result);
        }
    } catch (error) {
        console.error('Error updating changePassword:', error);
        res.status(500).json({ success: false, message: 'Error updating changePassword' });
    }
});

router.get("/logout", auth.verify, async (req, res) => {
    const userData = auth.decode(req.headers.authorization).id;
    const resultFromController = await userController.logoutUser(userData);
    res.send(resultFromController);
});

router.post("/login", async (req, res) => {
    try {
        const resultFromController = await userController.authenticateUser(req.body);
        res.send(resultFromController);
    } catch (error) {
        console.error("Error in login:", error);
        res.status(500).send("Internal server error");
    }
});

router.get("/details", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

router.post("/profile", (req, res) => {
    userController.userProfile(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/user-details", (req, res) => {
    userController.userDetails(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/all", (req, res) => {
    userController.allusers().then(resultFromController => res.send(resultFromController));
});

router.get("/alldetails", (req, res) => {
    userController.getAllUserDetails().then(resultFromController => res.send(resultFromController));
});

router.post("/checkEmail", (req, res) => {
    userController.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.put("/update", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    userController.updateProfile(userData.id, req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/department", (req, res) => {
    userController.getUserbyDepartment(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/taskTypes", (req, res) => {
    userController.getTaskTypebyDepartment(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/alltasktypes", (req, res) => {
    userController.getTaskTypes().then(resultFromController => res.send(resultFromController));
});

router.get("/getdepartment", (req, res) => {
    userController.getDepartment().then(resultFromController => res.send(resultFromController));
});


module.exports = router;
