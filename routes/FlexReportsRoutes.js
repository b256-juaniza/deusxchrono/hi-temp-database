const express = require('express');
const router = express.Router();
const upload = require('../config/multer'); // Ensure correct path to multer config
const reportController = require('../controllers/FlexReportsController');

// Route to handle file uploads using multer
router.post('/upload', upload.array('attachments', 10), async (req, res) => {
  try {
    // Extract URLs or paths of uploaded files
    const fileUrls = req.files.map(file => file.path);
    res.status(200).json({ fileUrls });
  } catch (error) {
    console.error('Error uploading files:', error);
    res.status(500).json({ error: 'Failed to upload files' });
  }
});

// Route to handle report creation with uploaded file URLs
router.post('/newReport', async (req, res) => {
  try {
    const { employeeName, department, data, fileUrls } = req.body;

    const report = await reportController.createReport({
      employeeName,
      department,
      data,
      fileUrls // Pass uploaded file URLs to controller
    });

    res.status(200).json(report);
  } catch (error) {
    console.error('Error creating report:', error);
    res.status(500).json({ error: 'Failed to create report' });
  }
});

router.put("/feedback", (req, res) => {
    reportController.updateFeedback(req.body).then(resultFromController => res.send(resultFromController));
});

router.delete("/deleteReport", (req, res) => {
    reportController.deleteReport(req.body).then(resultFromController => res.send(resultFromController));
});

router.post('/getreport', async (req, res) => {
    try {
        const { reportId } = req.body;
        const report = await reportController.getReport(reportId);
        res.status(200).json(report);
    } catch (error) {
        console.error('Error retrieving report:', error);
        res.status(500).json({ error: 'Failed to retrieve report' });
    }
});

// Route to get all reports by department
router.post('/bydepartment', async (req, res) => {
    try {
        const { department } = req.body;
        const reports = await reportController.getReportsByDepartment(department);
        res.status(200).json(reports);
    } catch (error) {
        console.error('Error retrieving reports:', error);
        res.status(500).json({ error: 'Failed to retrieve reports' });
    }
});

module.exports = router;
